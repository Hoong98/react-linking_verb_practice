//<---lottie json-->
import loading       from '../assets/lotties/loading.js';

//using_more_linking

import char1_default   from '../assets/lotties/game/char_1/default';
import char1_correct   from '../assets/lotties/game/char_1/correct';
import char1_wrong     from '../assets/lotties/game/char_1/wrong';

import char2_default   from '../assets/lotties/game/char_2/default';
import char2_correct   from '../assets/lotties/game/char_2/correct';
import char2_wrong     from '../assets/lotties/game/char_2/wrong';

import char3_default   from '../assets/lotties/game/char_3/default';
import char3_correct   from '../assets/lotties/game/char_3/correct';
import char3_wrong     from '../assets/lotties/game/char_3/wrong';

import char4_default   from '../assets/lotties/game/char_4/default';
import char4_correct   from '../assets/lotties/game/char_4/correct';
import char4_wrong     from '../assets/lotties/game/char_4/wrong';



const lotties={
  char1_default,
  char1_correct,
  char1_wrong,
  char2_default,
  char2_correct,
  char2_wrong,
  char3_default,
  char3_correct,
  char3_wrong,
  char4_default,
  char4_correct,
  char4_wrong,
  loading
}

export {lotties};
