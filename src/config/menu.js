
//pages
import Landing                      from '../pages/landing';
import Game                         from '../pages/game';
import Report                       from '../pages/report';


//menus images
import {img} from './media';


const menu = [
  {
    path:'/',
    preventMenu:true,
    component:()=><Landing/>
  },
  {
    path:'/game',
    preventMenu:true,
    component:()=><Game/>
  },
  {
    path:'/report',
    preventMenu:true,
    component:(props)=><Report {...props}/>
  },
];


export default menu;
