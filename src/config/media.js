
//<--------images-------->

//landing
import start_button             from '../assets/img/landing/start_button.png';
import start_button_clicked     from '../assets/img/landing/start-button-click.png';

import next_button from '../assets/img/universal/next_button.png'
import login_box   from '../assets/img/login/box_with_text_textbox.png'

//game
import placeholder_stone  from '../assets/img/game/blank_stone.png'
import board_game         from '../assets/img/game/board.png'
import fill_game          from '../assets/img/game/fill-in.png'
import fill_after         from '../assets/img/game/fill-in-after.png'
import stone              from '../assets/img/game/stone.png'
import try_again          from '../assets/img/game/try-again-button.png'

//report
import report_table from '../assets/img/report/question-board.png';
import print_button from '../assets/img/report/button-print.png';
import resul_box    from '../assets/img/report/name-board.png';
import audio_button from '../assets/img/report/button-audio-def.png';
import audio_play   from '../assets/img/report/button-audio.png';



//<----audio file---->
import game_start_audio from '../assets/img/game/game_start.mp3'
import back_audio       from '../assets/img/game/back_audio.mp3'

//report
import report_before from '../assets/img/report/before.mp3'

import q1_audio        from '../assets/img/report/question/Q1_audio.mp3'
import q2_audio        from '../assets/img/report/question/Q2_audio.mp3'
import q3_audio        from '../assets/img/report/question/Q3_audio.mp3'
import q4_audio        from '../assets/img/report/question/Q4_audio.mp3'
import q5_audio        from '../assets/img/report/question/Q5_audio.mp3'
import q6_audio        from '../assets/img/report/question/Q6_audio.mp3'
import q7_audio        from '../assets/img/report/question/Q7_audio.mp3'
import q8_audio        from '../assets/img/report/question/Q8_audio.mp3'
import q9_audio        from '../assets/img/report/question/Q9_audio.mp3'
import q10_audio       from '../assets/img/report/question/Q10_audio.mp3'
import q11_audio       from '../assets/img/report/question/Q11_audio.mp3'
import q12_audio       from '../assets/img/report/question/Q12_audio.mp3'
import q13_audio       from '../assets/img/report/question/Q13_audio.mp3'
import q14_audio       from '../assets/img/report/question/Q14_audio.mp3'
import q15_audio       from '../assets/img/report/question/Q15_audio.mp3'
import q16_audio       from '../assets/img/report/question/Q16_audio.mp3'
import q17_audio       from '../assets/img/report/question/Q17_audio.mp3'
import q18_audio       from '../assets/img/report/question/Q18_audio.mp3'
import q19_audio       from '../assets/img/report/question/Q19_audio.mp3'
import q20_audio       from '../assets/img/report/question/Q20_audio.mp3'
import q21_audio       from '../assets/img/report/question/Q21_audio.mp3'
import q22_audio       from '../assets/img/report/question/Q22_audio.mp3'
import q23_audio       from '../assets/img/report/question/Q23_audio.mp3'
import q24_audio       from '../assets/img/report/question/Q24_audio.mp3'
import q25_audio       from '../assets/img/report/question/Q25_audio.mp3'
import q26_audio       from '../assets/img/report/question/Q26_audio.mp3'


//<-----video file---->
import landing_back      from '../assets/img/landing/landing_back.mp4'
import game_back_loop    from '../assets/img/game/background_loop.mp4'


const img = {
    start_button,
    start_button_clicked,
    next_button,
    login_box,
    placeholder_stone,
    board_game,
    fill_game,
    fill_after,
    stone,
    try_again,
    report_table,
    print_button,
    resul_box,
    audio_button,
    audio_play
}


const audio = {
   game_start_audio,
   back_audio,
   report_before,
   q1_audio,
   q2_audio,
   q3_audio,
   q4_audio,
   q5_audio,
   q6_audio,
   q7_audio,
   q8_audio,
   q9_audio,
   q10_audio,
   q11_audio,
   q12_audio,
   q13_audio,
   q14_audio,
   q15_audio,
   q16_audio,
   q17_audio,
   q18_audio,
   q19_audio,
   q20_audio,
   q21_audio,
   q22_audio,
   q23_audio,
   q24_audio,
   q25_audio,
   q26_audio
  }

const video={
    landing_back,
    game_back_loop
}


export {img,audio,video};
