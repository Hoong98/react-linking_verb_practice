import  { useState,useEffect,useContext,useRef } from 'react';
import { Link,useHistory } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import SessionContext   from '../context/session_context';
import LottieAnimation    from '../components/lottie';
import moment           from 'moment';


import {Video} from '../components/components'


export default function Landing() {
  const [show,setShow]       = useState(false);
  const [start,setStart]     = useState(true);
  const [login,setLogin]     = useState(false);
  const [clicked,setClicked] = useState();
  const preContext           = useContext(PreLoaderContext);
  const video                = useRef();
  const history              = useHistory();
  const name_val             = useRef();
  const class_val            = useRef();
  const sessionContext       = useContext(SessionContext);

  let interval = null;

  function setSession() {
    if (name_val.current.value.trim() && class_val.current.value.trim()) {
      sessionContext.setSession({
        name:name_val.current.value,
        class:class_val.current.value,
        time:Date.now()
      })
      history.push('/game')
    }
  }

  function play() {
    setClicked(true);
    video.current.play()
  }

  function playing(e) {
    interval = setInterval(function () {
      if (e.target.currentTime >= 8) {
        setShow(true);
      }else if (e.target.currentTime >= 5) {
        setLogin(true)
        setStart(false)
      }
    }, 1000);
  }

  function looping(e) {
     e.target.currentTime = 11;
     video.current.play()
  }

  useEffect(()=>{
    return ()=>{
      clearInterval(interval)
    };
  },[])

  return <>
      <Video ref={video} src={preContext.videos.landing_back} preLoad={true} className="center absolute" style={style.full_video} onPlaying={playing} onEnded={looping}/>
      {login && <div className="absolute animated fadeIn"  style={style.landing_img}>
        <img src={preContext.images.login_box} className="absolute full-width" />
        <input ref={name_val} className="absolute center input coco" style={{top:'15%'}}/>
        <input ref={class_val} className="absolute center input coco" style={{top:'43%'}}/>
        <span className="absolute center input coco" style={style.date}>{moment().format('DD-MMM-YYYY')}</span>
      </div>}
      {start && <img src={clicked ? preContext.images.start_button_clicked : preContext.images.start_button} onClick={play} className="center pointer" style={style.center_button}/>}
      {show && <img src={preContext.images.next_button} onClick={setSession} className="center animated pointer fadeIn bottom-right" style={style.next_button} />}
  </>
}



const style = {
  landing_img:{
            position:'absolute',
            width:'50%',
            top:'27%',
            left: '0%',
            right:'0%',
            bottom:'0%',
            textAlign:'center',
            margin:'auto',
            height:'30%'
          },
          full_video:{
                  width:'100%',
                  left:0,
                  textAlign:'center',
        },
   center_button:{
     width:'20%',
     bottom:'-10%',
     zIndex:0
   },
   date:{
          top:'69%',
          paddingLeft:0,
          fontSize:'1.5vw',
        },
   next_button:{
     width:'13%'
   }
}
