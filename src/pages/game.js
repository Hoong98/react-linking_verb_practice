import  { useState,useEffect,useContext,useRef } from 'react';
import { Link,useHistory } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import SessionContext   from '../context/session_context';
import LottieAnimation    from '../components/lottie';
import moment           from 'moment';


import {Video,Audio,DragCard} from '../components/components'
import {lotties} from '../config/lotties_config';

const keySetter = (a) => a.map((a,b)=>({...a,key:b}))
function suffle(arr){
  return arr.sort((a,b)=> 0.5 - Math.random());
}

export default function Game() {
  const [ready,setReady]              = useState(false);
  const [show,setShow]                = useState(false);
  const preContext                    = useContext(PreLoaderContext);
  const [index,setIndex]              = useState(0);
  const [questionArr,setQuestionArr]  = useState(null);
  const [result,setResult]            = useState([]);
  const history                       = useHistory();
  const audio                         = useRef();
  let interval  = null;
  const ansArr               = {
                                  easy:keySetter([{text:'is'},{text:'are'},{text:'am'},{text:'has'},{text:'have'}]),
                                  high:keySetter([{text:'was'},{text:'were'},{text:'had'}])
                                }
  const lottieListLoad = [
    {animation:[lotties.char1_default,lotties.char1_correct,lotties.char1_wrong],time:5000},
    {animation:[lotties.char2_default,lotties.char2_correct,lotties.char2_wrong],time:5000},
    {animation:[lotties.char3_default,lotties.char3_correct,lotties.char3_wrong],time:5000},
    {animation:[lotties.char4_default,lotties.char4_correct,lotties.char4_wrong],time:5000},
  ];


  const lottieList = suffle([...suffle(lottieListLoad),...suffle(lottieListLoad)]);

  const QuestionBox = (props)=> <span style={{whiteSpace:'nowrap'}} className="animated fadeIn"><img src={props.correct ? preContext.images.fill_after :preContext.images.fill_game} /><span className="correct_ans animated fadeIn" style={{...props.style,...styleConst[props.text]}}>{props.text}</span></span>
  const QuestionBoxAnswer = (props)=> <span  className="green">{props.text}</span>

  const arr   = keySetter([
    {
      question:(correct)=><span className="absolute coco center question_text">I <QuestionBox correct={correct} style={style.smallCorrect} text="am"/> a nurse. I work at the hospital.</span>,
      answer :<span className="absolute coco">I <QuestionBoxAnswer answer={true}  style={style.smallCorrect} text="am"/> a nurse. I work at the hospital.</span>,
      answer_key:2,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">This bag <QuestionBox correct={correct} style={style.smallCorrect} text="is"/> big enough for me to fit all my books.</span>,
      answer :<span className="absolute coco">This bag <QuestionBoxAnswer answer={true} style={style.smallCorrect} text="is"/> big enough for me to fit all my books.</span>,
      answer_key:0,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The flowers <QuestionBox correct={correct} text="are"/>  very colourful and pretty.</span>,
      answer :<span className="absolute coco">The flowers <QuestionBoxAnswer answer={true} text="are"/>  very colourful and pretty.</span>,
      answer_key:1,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The dogs <QuestionBox correct={correct} text="have"/> soft, brown fur.</span>,
      answer :<span className="absolute coco">The dogs <QuestionBoxAnswer answer={true} text="have"/> soft, brown fur.</span>,
      answer_key:4,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">My favourite toy <QuestionBox correct={correct} style={style.smallCorrect} text="is"/> the robot.</span>,
      answer :<span className="absolute coco">My favourite toy <QuestionBoxAnswer answer={true} style={style.smallCorrect} text="is"/> the robot.</span>,
      answer_key:0,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The car <QuestionBox correct={correct} text="has"/> four wheels.</span>,
      answer :<span className="absolute coco">The car <QuestionBoxAnswer answer={true} text="has"/> four wheels.</span>,
      answer_key:3,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The lights <QuestionBox correct={correct} text="are"/> very bright.</span>,
      answer :<span className="absolute coco">The lights <QuestionBoxAnswer answer={true} text="are"/> very bright.</span>,
      answer_key:1,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Father reads the newspapers every morning. He <QuestionBox correct={correct} style={style.smallCorrect} text="is"/> interested in the Sports
section. </span>,
      answer :<span className="absolute coco">Father reads the newspapers every morning. He <QuestionBoxAnswer answer={true} style={style.smallCorrect} text="is"/> interested in the Sports
section. </span>,
      answer_key:0,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">I <QuestionBox correct={correct} text="have"/> many seashells at home because I love to collect them.</span>,
      answer :<span className="absolute coco">I <QuestionBoxAnswer answer={true} text="have"/> many seashells at home because I love to collect them.</span>,
      answer_key:4,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">I <QuestionBox correct={correct} style={style.smallCorrect} text="am"/> the tallest student in my class.</span>,
      answer :<span className="absolute coco">I <QuestionBoxAnswer answer={true} style={style.smallCorrect} text="am"/> the tallest student in my class.</span>,
      answer_key:2,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The children <QuestionBox correct={correct} text="have"/> sandwiches for recess.</span>,
      answer :<span className="absolute coco">The children <QuestionBoxAnswer answer={true} text="have"/> sandwiches for recess.</span>,
      answer_key:4,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">My cousins live in Malaysia. They <QuestionBox correct={correct} text="have"/> a big house there. </span>,
      answer :<span className="absolute coco">My cousins live in Malaysia. They <QuestionBoxAnswer answer={true} text="have"/> a big house there. </span>,
      answer_key:4,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Mother cooks every day. She <QuestionBox correct={correct} style={style.smallCorrect} text="is"/> a good cook.</span>,
      answer :<span className="absolute coco">Mother cooks every day. She <QuestionBoxAnswer answer={true} style={style.smallCorrect} text="is"/> a good cook.</span>,
      answer_key:0,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Petrina <QuestionBox correct={correct} text="has"/>  a pet rabbit. </span>,
      answer :<span className="absolute coco">Petrina <QuestionBoxAnswer answer={true} text="has"/>  a pet rabbit. </span>,
      answer_key:3,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">You <QuestionBox correct={correct} text="are"/> a helpful person. </span>,
      answer :<span className="absolute coco">You <QuestionBoxAnswer answer={true} text="are"/> a helpful person. </span>,
      answer_key:1,
      type:'easy',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">We <QuestionBox correct={correct} text="are"/>  going to the beach today.</span>,
      answer :<span className="absolute coco">We <QuestionBoxAnswer answer={true} text="are"/>  going to the beach today.</span>,
      answer_key:1,
      type:'easy',
    },
    //high
    {
      question:(correct)=><span className="absolute coco center question_text">The farmer <QuestionBox correct={correct} text="had"/> fed the animals this morning. </span>,
      answer :<span className="absolute coco">The farmer <QuestionBoxAnswer answer={true} text="had"/> fed the animals this morning. </span>,
      answer_key:2,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">We <QuestionBox correct={correct} text="were"/> at the airport last night.</span>,
      answer :<span className="absolute coco">We <QuestionBoxAnswer answer={true} text="were"/> at the airport last night.</span>,
      answer_key:1,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Mr Wong bought an umbrella as it <QuestionBox correct={correct} text="was"/> raining heavily just now. </span>,
      answer :<span className="absolute coco">Mr Wong bought an umbrella as it <QuestionBoxAnswer answer={true} text="was"/> raining heavily just now. </span>,
      answer_key:0,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The market  <QuestionBox correct={correct} text="was"/> very crowded two weeks ago.</span>,
      answer :<span className="absolute coco">The market  <QuestionBoxAnswer answer={true} text="was"/> very crowded two weeks ago.</span>,
      answer_key:0,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Yesterday, John and James went to the park. They <QuestionBox correct={correct} text="were"/> cycling.</span>,
      answer :<span className="absolute coco">Yesterday, John and James went to the park. They <QuestionBoxAnswer answer={true} text="were"/> cycling.</span>,
      answer_key:1,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Pui Ling <QuestionBox correct={correct} text="had"/> a pet cat when she was young.</span>,
      answer :<span className="absolute coco">Pui Ling <QuestionBoxAnswer answer={true} text="had"/> a pet cat when she was young.</span>,
      answer_key:2,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Last week, the young boy went to a birthday party. He <QuestionBox correct={correct} text="was"/> happy to receive a balloon at the end of it.</span>,
      answer :<span className="absolute coco">Last week, the young boy went to a birthday party. He <QuestionBoxAnswer answer={true} text="was"/> happy to receive a balloon at the end of it.</span>,
      answer_key:0,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">When Jamie found her favourite ring last week, she <QuestionBox correct={correct} text="was"/> delighted.</span>,
      answer :<span className="absolute coco">When Jamie found her favourite ring last week, she <QuestionBoxAnswer answer={true} text="was"/> delighted.</span>,
      answer_key:0,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">Alisah <QuestionBox correct={correct} text="was"/> four years old when he moved to another country with his family.</span>,
      answer :<span className="absolute coco">Alisah <QuestionBoxAnswer answer={true} text="was"/> four years old when he moved to another country with his family.</span>,
      answer_key:0,
      type:'high',
    },
    {
      question:(correct)=><span className="absolute coco center question_text">The man at the counter told us that we <QuestionBox correct={correct} text="were"/>  late for the show.</span>,
      answer :<span className="absolute coco">The man at the counter told us that we  <QuestionBoxAnswer answer={true} text="were"/>  late for the show.</span>,
      answer_key:1,
      type:'high',
    }
  ])

    function setSelect(a) {
      setResult([...result,a]);
      setShow(true);
    }


  function setNext() {
    if (index+1 !== questionArr.length) {
      setIndex(index+1);
    }else{
      history.push({pathname:'/report',state:{result,questionArr}})
    }
  }

  function playing() {
    interval = setInterval(()=>{
      if(audio.current && audio.current.currentTime >= 26){
        audio.current.volume=0.5;
      }
    }, 1000);
  }
  function playStart() {
    audio.current.volume=0.08;
  }
  useEffect(()=>{
    let hight_2 = suffle(arr.filter(a => a.type == "high")).slice(0,2);
    let easy_6  = suffle(arr.filter(a => a.type == "easy")).slice(0,6);

    setQuestionArr(
            suffle([...hight_2,...easy_6])
            .map((a,b)=>({
              ...a,
              ...lottieList[b],
              audio:preContext.audios[`q${a.key+1}_audio`]
            }))
          )
    return ()=>{
      clearInterval(interval)
    }
  },[])

  return <>
      <Video src={preContext.videos.game_back_loop} preLoad={true} loop={true} className="center absolute" style={style.full_video} autoPlay={true} mute={true}/>
      <Audio src={preContext.audios.game_start_audio} delay={100} onEnded={()=>setReady(true)} preLoad={true} autoPlay={true}/>
      <Audio ref={audio} onPlaying={playing} onPlay={playStart} src={preContext.audios.back_audio} delay={0} preLoad={true} autoPlay={true} loop={true}/>
      {questionArr && <Question key={index} ready={ready} index={index} data={questionArr[index]} ansArr={ansArr}  showNext={setShow} setSelect={setSelect}/>}
      {show && <img src={preContext.images.next_button} onClick={setNext} className="center pointer animated fadeIn bottom-right" style={style.next_button} />}
  </>
}


function Question(props) {
  const preContext     = useContext(PreLoaderContext);
  const data           = props.data;
  const [correct,setCorrect]   = useState(false);
  const [disabled,setDisabled] = useState(false);
  const [index,setIndex]       = useState(0);
  const [attempt,setAttempt]   = useState();
  const [tryShow,setTry]       = useState(false);
  const [key,setKey]           = useState(rand())

  let result    = {ans_key:'',ques_key:''};

  function check(a,elem) {
    setIndex(a.correct ? 1 : 2);
    if (!disabled) {
      setDisabled(true)
          result = {
            ans_key:a.ans_key,
            ques_key:a.ques_key,
            correct:a.correct,
            score:a.correct && attempt == 2 ? 2 : (a.correct && attempt == 1 ? 1 : 0)
          }
          setTimeout(()=>{
            if (a.correct) {
              setCorrect(true)
            }
          },2700)
          setTimeout(()=> {
            if (!a.correct && attempt == 2) {
                  setTry(true);
                  setAttempt(attempt-1)
              }else{
                props.setSelect(result);
            }
          }, data.time);
    }
  }
  useEffect(()=>{
    props.showNext(false)
    setDisabled(false)
    setAttempt(2)
  },[])

  function resetAll() {
    setDisabled(false)
    setTry(false);
    setIndex(0);
    setKey(rand())
  }
  return <div key={key}>
        <LottieAnimation data={data.animation[index]} loop={index == 0} style={style.lottie}/>
        <div className="center  absolute"  draggable={false} style={style.top_center} >
          <span className="absolute coco absolute question_title text-color animated fadeIn">Question {props.index+1} of 8 </span>
          {data.question(correct)}
          <img src={preContext.images.board_game} className="absolute full-width" />
        </div>
        <div className="absolute ans_box_container center" draggable={false}>
          {props.ansArr && props.ansArr[data.type].map((a,b)=>{
            let left = (b+1)*11;
            return  <DragCard disabledTime={3000} ready={props.ready} prevent={disabled} data={{
                ques_key:data.key,
                ans_key :a.key,
                correct :(data.answer_key == a.key)
              }} text={a.text} key={left} check={check} selected={a.key} src={preContext.images.stone} style={{left:left+'%'}} dropClass="placeholder_box" />
          })}
        </div>
        <div className="absolute full-width placeholder_box center">
          <img src={preContext.images.placeholder_stone} style={style.placeholder_stone}  className="center after_hide full-width absolute" />
          {props.ansArr && props.ansArr[data.type].map((a,b)=>{
            return <DummyPlacer text={a.text} key={b} id={'ans_id'+b} src={preContext.images.stone} className="dummy_placer" />
          })}
        </div>
        {tryShow && <img src={preContext.images.try_again} className="center pointer animated fadeIn bottom-right" style={style.try_button} onClick={resetAll} />}
  </div>;
}

function rand() {
  return 'key_rest'+Math.round(Math.random()*10000);
}

function DummyPlacer(props) {
    const preContext  = useContext(PreLoaderContext);
    return <div id={props.id} className={props.className}>
      <span className="absolute center coco text-white">{props.text}</span>
    </div>;
}

const styleConst = {
  is:{
    padding: '0.3% 0%',
    marginLeft: '-7.5%',
    marginRight: '5%'
  },
  am:{
    padding: '0.3% 0%',
    marginLeft: '-8.5%',
    marginRight: '4%'
  },
  are:{
    marginLeft: '-9%',
  },
  has:{
    marginLeft: '-9%',
  },
  had:{
    marginLeft: '-9%',
  },
  was:{
    marginLeft: '-9.5%',
  }
}

const style = {
  landing_img:{
            position:'absolute',
            width:'50%',
            top:'27%',
            left: '0%',
            right:'0%',
            bottom:'0%',
            textAlign:'center',
            margin:'auto',
            height:'30%'
          },
          full_video:{
                  width:'100%',
                  left:0,
                  textAlign:'center',
        },
   center_button:{
     width:'20%',
     bottom:'-10%',
     zIndex:0
   },
   lottie:{
     width:'93%',
     left:'4%',
     top:'3%'
   },
   date:{
          top:'69%',
          paddingLeft:0,
          fontSize:'1.5vw',
        },
   next_button:{
     width:'13%'
   },
   top_center:{
    left:'0%',
    top:'-7%',
    width:'80%'
  },
  placeholder_stone:{
    zIndex: 0,
    width: '72%',
    left: '-7%',
    top: '22%'
  },
  try_button:{
     width:'13%',
     zIndex:3,
     bottom:'2%',
     height:'10%'
   },
   smallCorrect:{
     padding:'0% 0%'
   }
}
