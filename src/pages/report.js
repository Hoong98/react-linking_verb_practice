import  { useState,useEffect,useContext,useRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import SessionContext   from '../context/session_context';
import LottieAnimation    from '../components/lottie';
import moment           from 'moment';

import {Audio,Video,Passer} from '../components/components'

import {lotties} from '../config/lotties_config';


export default function Report(props) {
  const preContext            = useContext(PreLoaderContext);
  const sessionContext        = useContext(SessionContext);
  const [data,setData]        = useState(null);
  const [audio,setAudio]      = useState();
  const [total,setTotal]      = useState(4);
  const [play,setPlay]        = useState(false);
  const audioRef              = useRef();

  useEffect(()=>{
    if (props.location.state) {
      let pre_result   = props.location.state.result;
      let question     = props.location.state.questionArr;
      let resultArr    = [];
      let total        = 0;
      pre_result.forEach((a)=>{
        let ind   = question.findIndex(i=>i.key == a.ques_key);
        let arr   = question[ind];
        arr.score = a.score;
        total +=parseFloat(a.score)
        resultArr.push(arr)
      })
      setTotal(total)
      setData(resultArr);
    }
  },[])

  function playAudio(a) {
    audioRef.current.src=a;
    audioRef.current.load();
    audioRef.current.play();
    setPlay(true)
  }
  return <>
      {data && <Audio src={preContext.audios.report_before} preLoad={true} delay={0} autoPlay={true}   />}
      <Audio ref={audioRef} delay={0} onEnded={()=>setPlay(false)}/>
      <Video src={preContext.videos.game_back_loop}   style={style.full_video} preLoad={true} className="absolute" delay={0} mute={true} autoPlay={true} loop={true}  />
    <div className="absolute" style={{...style.landing_img,height:'100%'}}>
      <img src={preContext.images.print_button} onClick={()=>window.print()} className="absolute  pointer prevent_print"  style={style.print_button}/>
    {sessionContext.session && <><span className="absolute coco"   style={{...style.date,left:'29.3%',top: '10.2%'}}>{sessionContext.session.name}</span>
      <span className="absolute coco" style={{...style.date,left:'29.3%',top: '15.7%'}}>{sessionContext.session.class}</span>
      <span className="absolute coco" style={{...style.date,left:'55.3%',top:'10.7%'}}>{moment(sessionContext.session.time).format('DD-MMM-YYYY')}</span></>}
      {data && <span className="absolute coco" style={style.mark}>{total}</span>}
      <img src={preContext.images.resul_box} className="absolute center"  style={style.header_box}/>
      <img src={preContext.images.report_table} className="absolute center"  style={style.result_container}/>
      <ul className="absolute report_table">
       {data && data.map((a,b)=>{
          return a && <li key={'loop_'+b.toString()} style={{top:((b+1)*12)+'%'}}>
            <span className="absolute sr_no coco">{b+1}</span>
            <span className="text_box coco">{a.answer}</span>
            <span className="text_box_answer coco absolute ">{a.score}</span>
            <AudioPlay onEnd={play} playAudio={()=>{playAudio(a.audio);}}  />
            </li>
        })  }
      </ul>
      </div>
  </>
}

function AudioPlay(props) {
  const preContext            = useContext(PreLoaderContext);
  const [play,setPlay]        = useState(false);
  useEffect(()=>{
    if (!props.onEnd) {
      setPlay(false)
    }
  },[props.onEnd])

  useEffect(()=>{
    return ()=>{
      setPlay(false)
    }
  },[])

  function playStart() {
    props.playAudio();
    setPlay(true);
  }
  return <img src={play ? preContext.images.audio_play: preContext.images.audio_button} onClick={playStart} className="speaker pointer absolute"/>;
}



const style = {
  landing_img:{
            position:'absolute',
            width:'100%',
            top:'0%',
            left: '0%'
          },
    full_video:{
                    width:'100%',
                    left:0,
                    textAlign:'center',
          },
    header_box:{
      width:'60%',
      top:'5%'
    },
    result_container:{
      width:'80%',
      top:'30%'
    },
    pdf_button:{
      left:'67%',
      top:'10.5%',
      width:'4%',
      zIndex:'2',
    },
    date:{
      left:'53.3%',
      top:'11%',
      width:'10%',
      fontSize:'1.2vw',
      zIndex:'2',
      fontWeight:'500'
    },
    mark:{
      left:'56%',
      top:'14.5%',
      width:'10%',
      fontSize:'2vw',
      zIndex:'2',
      fontWeight:'500'
    },
    print_button:{
      left:'70%',
      top:'10.5%',
      width:'4%',
      zIndex:'2',
    }

}
