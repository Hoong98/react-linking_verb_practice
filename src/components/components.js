import  { useState,useRef,useEffect,forwardRef,useContext} from 'react';
import PreLoaderContext from '../context/preload_context';
import Draggable from 'react-draggable';


export const Audio = forwardRef((props,ref) =>{
  const [show,setShow] = useState(false);

  useEffect(()=>{
    setTimeout(()=>{
      setShow(true)
    },props.delay);
  },[props.delay])

  return show && <audio preload={"true"} style={props.style} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} onEnded={props.onEnded} loop={props.loop} onPlaying={props.onPlaying} onPlay={props.onPlay} type="audio/mpeg"></audio>
})

function rand() {
  return 'key_rest'+Math.round(Math.random()*10000);
}

export  function DragCard(props) {
  const nodeRef = useRef(null);
  const [key,setKey] = useState(rand());

  const onStop = (e) =>{
    e.preventDefault();
    if (e.target.classList.contains(props.dropClass) || e.target.parentElement.classList.contains(props.dropClass)) {
      document.querySelector('.placeholder_box').classList.add('filled');
      document.querySelector(`#ans_id${props.selected}`).classList.add('show');
      document.querySelector('.after_hide').classList.add('hide');
      setTimeout(function () {
        if (props.data.correct) {
          document.querySelector('.question_text').classList.add('show');
        }else {
          document.querySelector('.placeholder_box').classList.add('hide');
        }
      }, props.disabledTime);
      props.check(props.data,nodeRef);
      nodeRef.current.style.transform='translate(0px, 0px)';
      nodeRef.current.remove()
    }else{
      nodeRef.current.style.transform='translate(0px, 0px)';
       setTimeout(function () {
         setKey(rand())
       }, 100);
    }
  }
  return <Draggable axis="both" disabled={!props.ready} key={key} onStart={props.onStart} nodeRef={nodeRef} handle=".drags" cancel={props.prevent ? ".drags":""} onStop={onStop}  defaultPosition={{x:0, y: 0}}  grid={[25, 25]} scale={1}>
          <div className="absolute ans_box drags" ref={nodeRef} draggable={false} style={props.style}>
            <img src={props.src} draggable={false}  className="absolute full-width" />
            <span className="absolute center coco text game_question">{props.text}</span>
          </div>
    </Draggable>;
}



export function Passer(props){
  const preContext        = useContext(PreLoaderContext);
  const [imgView,setImg]  = useState(props.imgView);

  useEffect(()=>{ setImg(props.imgView); },[props.imgView])
  const switchCall = () =>{
    if (!props.prevent && imgView != 0) {
      setImg(imgView == 1 ? 2:1);
    }
  }
  return  props.switcher ? <img src={props.switcher[imgView]} onClick={switchCall} className="absolute animated fadeIn pointer" style={{...style.top_center,...(props.prevent ? {left:'23%',width:'45%'}:{})}} />:null;
}


export const Video = forwardRef((props,ref) =>{
  const [show,setShow] = useState(false);
  useEffect(()=>{
    setTimeout(()=>{
      setShow(true)
    },props.delay);
  },[props.delay])

  return show && <video  preload={"true"} onPlaying={props.onPlaying}  style={props.style} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} loop={props.loop} onEnded={props.onEnded} onPlay={props.onPlay} type="audio/mpeg"></video>
})



const style = {
  top_center:{
    left:'18%',
    top:'7%',
    width:'50%'
  }
}
