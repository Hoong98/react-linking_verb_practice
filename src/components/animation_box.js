import  { useState,useEffect,useContext,createRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';

//component
import {Video,Passer} from '../components/components'


export default function AnimationBox(props) {
  const preContext                        = useContext(PreLoaderContext);
  const [show,setShow]                    = useState(false);
  const [nextComponent,setNextComponent]  = useState(false);
  const [nextPath,setNext]                = useState();
  const ref                               = createRef();
  let interval                            = '';
  const [imgView,setImg]                  = useState(0);

  function videoEnd() {
    if (props.data.nextComponent) {
       setNextComponent(true);
    }else{
      if (!props.preventNext) {
        setShow(true);
      }
    }
  }

  const detectTime        = (e) => {
    if (!props.data.prevent) {
      interval = setInterval(()=>{
        if(e.target.currentTime >= props.data.secondImgeTime){
          setImg(1);
        }
      },1000)
    }
  }
  useEffect(()=>{
    if (!props.singlePath) {
        if (props.index == props.maxIndex) {
          setNext(props.nextPath);
        }else{
            setNext(`${props.currentPath}/${props.index+1}`);
        }
    }else{
      setNext(props.nextPath);
    }
    return ()=>{
      if (!props.data.prevent && interval) {
        clearInterval(interval)
      }
    }
  },[])
  return nextComponent ? props.data.nextComponent : <>
              <Video ref={ref} src={props.data.video} onPlaying={detectTime} controls={true} style={style.full_video} preLoad={true} className="absolute" onEnded={videoEnd} delay={500} autoPlay={true} />
              <Passer imgView={imgView} prevent={props.data.prevent} switcher={props.data.switcher}/>
              {show && <Link to={nextPath}><img src={preContext.images.next_button} className="center bottom-right" style={style.next_button} /></Link>}</>
}

const style = {
  full_video:{
            width:'100%',
            left:0,
            textAlign:'center',
          },
   top_center:{
     left:'16%',
     top:'7%',
     width:'60%'
   },
   next_button:{
     width:'15%'
   }
}
